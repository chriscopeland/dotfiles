set nocompatible

set smartindent
set expandtab
set shiftwidth=2
set tabstop=2

set hlsearch
set ignorecase
set incsearch
set smartcase

set timeoutlen=1000
set ttimeoutlen=0

let $BASH_ENV = "~/.bash_aliases"

set list
set listchars=tab:>-,trail:~,extends:>,precedes:<

syntax on
filetype plugin indent on

autocmd FileType python,json,javascript,proto
  \ set shiftwidth=4 tabstop=4

autocmd FileType go
  \ set shiftwidth=8 tabstop=8

autocmd FileType verilog
  \ set nosmartindent

autocmd FileType go,gitconfig,make
  \ set noexpandtab listchars=tab:\ \ ,trail:~,extends:>,precedes:<

autocmd FileType c,cpp,haskell,verilog,ruby,gitconfig,make,json,javascript,matlab,python,asm,ld
  \ autocmd BufWritePre <buffer> :%s/\s\+$//e

" disable repeating the comment leader for single line comments in C/C++
autocmd FileType c,cpp setlocal comments-=:// comments+=f://

autocmd BufNew,BufRead SConstruct,SConscript setf python

set mouse=
set backspace=indent,eol,start

call pathogen#infect()

" for airline
set laststatus=2
set noshowmode
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_alt_sep = ''
"let g:airline_left_sep = ''
"let g:airline_left_alt_sep = ''
"let g:airline_right_sep = ''
"let g:airline_right_alt_sep = ''
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '␤'
"let g:airline_symbols.linenr = '¶'
let g:airline_symbols.linenr = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.paste = 'ρ'
"let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" allow spaces after tabs, but not in between
let g:airline#extensions#whitespace#mixed_indent_algo = 1

let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
nnoremap <C-w>e :SyntasticToggleMode<CR> :SyntasticCheck<CR>
nnoremap <C-w>r :SyntasticReset<CR> :SyntasticToggleMode<CR>

let g:syntastic_error_symbol = "✗"
let g:syntastic_warning_symbol = "⚠"
let g:syntastic_style_error_symbol = "s✗"
let g:syntastic_style_warning_symbol = "s⚠"
let g:syntastic_always_populate_loc_list = 1

" Settings for vim-go: https://github.com/fatih/vim-go
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>l <Plug>(go-metalinter)
au FileType go nmap <leader>c <Plug>(go-coverage)
au FileType go nmap <leader>d <Plug>(go-def)
"au FileType go nmap <Leader>ds <Plug>(go-def-split)
"au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
"au FileType go nmap <Leader>dt <Plug>(go-def-tab)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <Leader>gb <Plug>(go-doc-browser)
au FileType go nmap <Leader>s <Plug>(go-implements)
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>e <Plug>(go-rename)
let g:go_highlight_functions = 1
" let g:go_highlight_methods = 1
" let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_cgo = 1
let g:go_fmt_command = "goimports"
" let g:go_fmt_fail_silently = 1
let g:go_highlight_extra_types = 0
let g:go_list_type = "quickfix"
let g:go_version_warning = 0

" For C, C++, etc., use clang-format
let g:clang_format#code_style = "file"

nmap <F8> :TagbarToggle<CR>

let g:autopep8_disable_show_diff=1
