# Source global definitions

sources=(
  "/etc/bashrc"
  "/etc/bash.bashrc"
  "/etc/profile"
  "/etc/bash_completion"
  "$HOME/.bash_aliases"
  "$HOME/.fzf.bash"
)

for p in ${sources[@]}; do
  if [ -f "$p" ]; then
    source "$p"
  fi
done

paths=(
  "/usr/local/opt/python/libexec/bin"
  "/usr/texbin"
  "/usr/local/go/bin"
  "/usr/local/sbin"
  "$HOME/go/bin"
  "$HOME/.cabal/bin"
  "$HOME/.cargo/bin"
  "$HOME/.local/bin"
)

for p in ${paths[@]}; do
  if [ -d "$p" ]; then
    export PATH="$p:$PATH"
  fi
done

umask 0022

export PS1="\n\h:\W\$ "

# Use a symlink to automatically keep SSH_AUTH_SOCK up to date.
if [ -n "$SSH_CONNECTION" ]; then
  socklink="$HOME/.ssh/ssh_auth_sock"
  # If SSH_AUTH_SOCK isn't already the symlink, it was set
  # by a new ssh session, so the symlink should be updated.
  if [ ! "$socklink" = "$SSH_AUTH_SOCK" ]; then
    ln -sf "$SSH_AUTH_SOCK" "$socklink"
    export SSH_AUTH_SOCK="$socklink"
  fi
fi

# macOS settings
if [ "$(uname -s)" = "Darwin" ]; then
  # Homebrew
  export HOMEBREW_GITHUB_API_TOKEN="3ff9738ee009761bedd44592c7e3ec6be7030a3d"
  export HOMEBREW_NO_EMOJI=1

  # For color output of ls
  export LSCOLORS="gxexbxdxcxegedxbxgxcxd"

  function clip {
    [ -t 0 ] && pbpaste || pbcopy
  }

  export BASH_COMPLETION_COMPAT_DIR="/usr/local/etc/bash_completion.d"
  bash_completion="/usr/local/etc/profile.d/bash_completion.sh"
  [[ -r "$bash_completion" ]] && . "$bash_completion"

elif [ "$(uname -s)" = "Linux" ]; then
  function clip {
    [ -t 0 ] && xclip -o -selection clipboard || xclip -i -selection clipboard
  }
fi

if [ -f ~/.ssh/id_ed25519 ]; then
  eval "$(keychain --quiet --agents ssh --eval id_ed25519)"
fi
