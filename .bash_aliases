shopt -s expand_aliases

uname="$(uname -s)"

# macOS aliases
if [ "$uname" == "Darwin" ]; then
  # For color output of ls
  alias ls="ls -G"
  alias updatedb="sudo /usr/libexec/locate.updatedb"

  # Homebrew
  alias brewup="brew update && brew upgrade && brew cleanup"

  alias nproc="sysctl -n hw.ncpu"

# Linux (debian) aliases
elif [ "$uname" == "Linux" ]; then
  alias aptup="sudo apt -qq update && sudo apt -qq full-upgrade && sudo apt -qqq autoremove"
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
  alias open='xdg-open'
fi

alias matlab="matlab -nojvm"
alias diffstat="diffstat -C"
alias scons="scons --jobs $(nproc)"
alias gsuri="git submodule update --recursive --init"
